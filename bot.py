import discord
import datetime
import sys
from discord.ext import commands
from botconfig import token, prefix, botdesc, botversion, since, owner



intents = discord.Intents.default()
intents.message_content = True

bot = commands.Bot(command_prefix=prefix, description=botdesc, owner_id=owner, intents=intents)


@bot.event
async def on_ready():
    print('=================')
    print('Logged in as:')
    print(bot.user.name)
    print(bot.user.id)
    print('=================')
    print(f'Bot Owner: {bot.owner_id}')


@bot.command()
async def info(ctx):
    """Displays Information about the Bot in its current state."""

    author_repo = "https://gitlab.com/Cabooshyy"
    nep_repo    = author_repo + "/CPU-Purple-Heart"
    dpy_repo    = "https://github.com/Rappyz/discord.py"
    python_url  = "https://www.python.org/"
    days_since  = (datetime.datetime.now() - since).days
    dpy_version = f'[{discord.__version__}]({dpy_repo})'
    python_version = "[{}.{}.{}]({})".format(*sys.version_info[:3], python_url)
    nep_version = f'[ Ver. {botversion} ]'
    app_info = await bot.application_info()
    owner = app_info.owner
    about = (
        f'This is an instance of [CPU Purple Heart, an Open Source Discord Bot]({nep_repo}) '
        f'Created by [Cabooshy]({author_repo})')

    embed = discord.Embed(color=discord.Colour(0xb675c7))
    embed.set_author(name="CPU Purple Heart", icon_url=bot.user.avatar.url)
    embed.set_thumbnail(url=bot.user.avatar.url)
    embed.add_field(name="Instance Owned By", value=str('`{}`'.format(owner)))
    embed.add_field(name="Python", value=python_version)
    embed.add_field(name="discord.py", value=dpy_version)
    embed.add_field(name="Neptune's version", value=nep_version)
    embed.add_field(name="About Neptune", value=about, inline=False)
    embed.set_footer(
        text=f"Breaking the 4th Wall on Discord since 29 Dec 2015 ({days_since} days ago!)"
        , icon_url=bot.user.avatar.url)
    try:
        await ctx.send(embed=embed)
    except discord.HTTPException:
        await ctx.send("I need Embed Perms to send this.")


bot.run(token)
